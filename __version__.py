"""
Manages the version number for the project based on git tags.
If on a tag, report that as-is.
When moved on from the tag, auto-increment the desired level of semantic verion
"""
import re
import os
import sys
import datetime
import subprocess
from pathlib import Path
import traceback

SUPPORT_PATCH = False

# Set environment variable "VERSION_INCREMENT" to set next version jump
VERSION_INCREMENT_ENV = "VERSION_INCREMENT"

PROJECT_ROOT_ENV = "PROJECT_ROOT"

VERSION_INCREMENT_PATCH = 'patch'
VERSION_INCREMENT_MINOR = 'minor'
VERSION_INCREMENT_MAJOR = 'major'

DEFAULT_INCREMENT = VERSION_INCREMENT_PATCH if SUPPORT_PATCH else VERSION_INCREMENT_MINOR

try:
    repo_dir = os.environ.get(PROJECT_ROOT_ENV, os.path.dirname(__file__)) or '.'
except NameError:
    repo_dir = "."
    __file__ = None



VERBOSE = False


def vers_split(vers):
    try:
        return list(re.search(r"v?(\d+\.\d+(\.\d+)?)", vers).group(1).split('.'))
    except:
        print("Could not parse version from:", vers, file=sys.stderr)
        raise


def get_version_info_from_git():
    fail_ret = None, None, None, None
    # Note: git describe doesn't work if no tag is available
    try:
        git_tag = subprocess.check_output(["git", "describe", "--long", "--tags", "--dirty", "--always"],
                                          cwd=repo_dir, stderr=subprocess.STDOUT, universal_newlines=True).strip()
    except subprocess.CalledProcessError as er:
        if VERBOSE:
            traceback.print_exc()
        if er.returncode == 128:
            # git exit code of 128 means no repository found
            return fail_ret
        git_tag = ""
    except OSError:
        if VERBOSE:
            traceback.print_exc()
        return fail_ret

    parts = re.match(r'(^.*?)-(\d+)-g(.*?)(-dirty)?', git_tag)
    tag_name = parts.group(1)
    num_commits = parts.group(2)
    git_hash = parts.group(3)
    git_dirty = parts.group(4)

    git_tag_parts = vers_split(git_tag)

    try:
        # Find all tags on the commit and get the largest version if there are multiple
        tag_sha = subprocess.check_output(["git", "rev-list", "-n", "1", tag_name],
                                          cwd=repo_dir, stderr=subprocess.STDOUT, universal_newlines=True).strip()

        sha_tags = subprocess.check_output(["git", "tag", "--contains", tag_sha],
                                           cwd=repo_dir, stderr=subprocess.STDOUT, universal_newlines=True).strip()

        sha_tags = [vers_split(t) for t in sha_tags.split('\n')]
        git_tag_parts = max(sha_tags)

    except subprocess.CalledProcessError as er:
        if VERBOSE:
            traceback.print_exc()

    except OSError:
        if VERBOSE:
            traceback.print_exc()
        return fail_ret

    try:
        on_tag = subprocess.check_output(["git", "describe", "--exact-match", "--tags", "HEAD"],
                                         cwd=repo_dir, stderr=subprocess.STDOUT, universal_newlines=True).strip()
    except subprocess.CalledProcessError:
        if VERBOSE:
            traceback.print_exc()
        on_tag = False
    except OSError:
        if VERBOSE:
            traceback.print_exc()
        return fail_ret

    if git_dirty:
        git_hash += "-dirty"

    return git_tag_parts, tag_name, git_hash, on_tag


def increment_index(increment):
    try:
        index = {
            VERSION_INCREMENT_PATCH: 2,
            VERSION_INCREMENT_MINOR: 1,
            VERSION_INCREMENT_MAJOR: 0,
        }[increment]
    except KeyError:
        raise SystemExit("change: %s must be one of '%s', '%s' or '%s'" %
                            (increment, VERSION_INCREMENT_MAJOR,
                            VERSION_INCREMENT_MINOR, VERSION_INCREMENT_PATCH))
    return index


def increment_from_messages(tag_name):
    # Increment version
    increment = []

    # Check git logs between last tag and now
    try:
        commit_messages = subprocess.check_output(["git", "log", "%s..HEAD" % tag_name],
                                cwd=repo_dir, stderr=subprocess.STDOUT, universal_newlines=True).strip()
    except subprocess.CalledProcessError:
        commit_messages = ''

    for match in re.findall(r'CHANGE: *(%s|%s|%s)' % (
        VERSION_INCREMENT_MAJOR, VERSION_INCREMENT_MINOR, VERSION_INCREMENT_PATCH
    ), commit_messages):
        try:
            increment.append(increment_index(match))
        except SystemExit as ex:
            print(ex.args, file=sys.stderr)
    if increment:
        return min(increment)
    return None


def git_version():
    parts, tag_name, git_hash, on_tag = get_version_info_from_git()
    if not parts:
        raise ValueError("could not read git details")
    try:
        if not on_tag:

            index = increment_from_messages(tag_name)

            # Fallback to checking env for increment if commit messages don't specify
            if index is None:
                increment = os.environ.get(VERSION_INCREMENT_ENV, DEFAULT_INCREMENT).lower()
                if len(parts) < 2:
                    if VERBOSE:
                        print("WARNING: Adding minor version to scheme that previously had none", file=sys.stderr)
                    parts.append('0')
                elif len(parts) < 3 and SUPPORT_PATCH:
                    if VERBOSE:
                        print("WARNING: Adding patch version to scheme that previously had none", file=sys.stderr)
                    parts.append('0')

                index = increment_index(increment)

            if index == increment_index(VERSION_INCREMENT_PATCH) and not SUPPORT_PATCH:
                raise SystemExit("Increment '%s' not currently supported" % VERSION_INCREMENT_PATCH)

            max_index = 2 if SUPPORT_PATCH else 1

            parts = parts[0:index] + [str(int(parts[index]) + 1)] + (['0'] * max(0, (max_index - index)))

    except (IndexError, ValueError, AttributeError) as ex:
        if "'NoneType' object has no attribute 'group'" in str(ex):  # regex fail
            print("Parsing version number failed:", tag_name, file=sys.stderr)
        else:
            print("Could not increment %s : %s" % (tag_name, ex), file=sys.stderr)

    vers_short = "v" + ".".join(parts)
    vers_long = vers_short + '-g' +  git_hash
    return vers_short, vers_long


version = "UNKNOWN"
version_short = "0.0.0" if SUPPORT_PATCH else "0.0"

try:
    from __version import version, version_short
except:
    pass

error = None
if __file__ is not None:
    try:
        version_short, version = git_version()
        (Path(__file__).parent / '__version.py').write_text(f'version = "{version}"\nversion_short = "{version_short}"')
    except Exception as ex:
        error = str(ex)


def rename_file(pattern):
    import glob
    for f in glob.glob(pattern):
        f = Path(f)
        newname = f.name.format(
            version=version,
            version_short=version_short,
        )
        print(f'Renaming "{f}" -> "{newname}"')
        f.rename(f.with_name(newname))


if __name__ == "__main__":
    VERBOSE = True
    if error:
        try:
            version_short, version = git_version()
        except Exception as ex:
            import traceback
            traceback.print_exc()
    if sys.argv[-1].lower() == "--short":
        print(version_short)
    elif "--rename" in sys.argv:
        rename_file(sys.argv[-1])
    else:
        print(version)
