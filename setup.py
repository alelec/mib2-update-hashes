import os
import re
from setuptools import setup
from __version__ import local_scheme, version_scheme

setup(
    name="mib2-update-hashes",
    author="Andrew Leech",
    author_email="andrew@alelec.net",
    description="Tool to rebuild modified metainfo2.txt for MIB2 Harman (High) units",
    py_modules=['update_hashes.py'],
    use_scm_version = {
        "local_scheme": local_scheme,
        "version_scheme": version_scheme
    },
    setup_requires=['setuptools_scm', 'pyoxidizer'],
    install_requires=[
        "configobj==5.0.6",
    ]
)
